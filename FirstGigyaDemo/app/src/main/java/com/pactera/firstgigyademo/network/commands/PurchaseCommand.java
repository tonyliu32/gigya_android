package com.pactera.firstgigyademo.network.commands;


import com.pactera.firstgigyademo.GigyaSampleLog;
import com.pactera.firstgigyademo.ICallback;
import com.pactera.firstgigyademo.ICommand;

public class PurchaseCommand implements ICommand {

    private static final String TAG = "PurchaseCommand";
    private String mUserId;
    private String mProductId;
    private ICallback<String, Exception> mCallback;

    public PurchaseCommand(String userId, String productId, ICallback<String, Exception> callback) {
        mUserId = userId;
        mProductId = productId;
        mCallback = callback;
    }

    @Override
    public void execute() {
//        IOfirStoreAPI api = RetrofitClient.getClient().create(IOfirStoreAPI.class);
//        PurchaseBody body = new PurchaseBody();
//        body.productId = mProductId;
//        body.userId = mUserId;
//        Call<OfirResponse> response = api.purchaseItem(body);
//        response.enqueue(new Callback<OfirResponse>() {
//            @Override
//            public void onResponse(Call<OfirResponse> call, Response<OfirResponse> response) {
//                if (response != null && response.body() != null && response.body().getId() == 0) {
//                    GigyaSampleLog.i(TAG, "response.body().getId() " + response.body().getId());
//                    int id = response.body().getId();
//                    GigyaSampleLog.i(TAG, "onResponse with id " + id);
//                    //Success
//                    if (id == 0) {
//                        String message = response.body().getMessage();
//                        GigyaSampleLog.i(TAG, "onResponse with message " + message);
//                        mCallback.onSuccess(message);
//                    } else {
//                        mCallback.onError(new Exception("Error: purchase failed"));
//                    }
//                } else {
//                    mCallback.onError(new Exception("Error: purchase failed"));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<OfirResponse> call, Throwable t) {
//                mCallback.onError(new Exception("Error: purchase failed"));
//            }
//        });
    }
}
