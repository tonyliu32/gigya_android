package com.pactera.firstgigyademo.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.pactera.firstgigyademo.R;
import com.pactera.firstgigyademo.model.User;
import com.pactera.firstgigyademo.ui.UIResponse;
import com.pactera.firstgigyademo.ui.viewmodel.AccountViewModel;

public class MyAccountActivity extends AppCompatActivity {

    private AccountViewModel mAccountViewModel;
    private TextInputEditText txt_fname;
    private TextInputEditText txt_lname;
    private Button btn_update;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        mAccountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);


        btn_update = findViewById(R.id.btn_update);

        User user = getIntent().getParcelableExtra("user");
        mAccountViewModel.setCurrentUser(user);
        getUserData(user);

        btn_update.setOnClickListener(
                new View.OnClickListener(){

                    @Override
                    public void onClick(View v) {

                        txt_fname = findViewById(R.id.txt_fname);
                        txt_lname = findViewById(R.id.txt_lname);
                        String firstName = txt_fname.getText().toString();
                        String lastName = txt_lname.getText().toString();
                        if(firstName != null && lastName != null){

                            mAccountViewModel.getAccountLiveData();
                            mAccountViewModel.setAccountDetails(firstName, lastName);
                            showWelcomeActivity();
                        }


                    }
                }
        );

    }

    private void showWelcomeActivity() {

        Intent welcomeIntent = new Intent(this, WelcomeActivity.class);
        mAccountViewModel.setCurrentUser(null);
        startActivity(welcomeIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void getUserData(User u) {
        txt_fname = findViewById(R.id.txt_fname);
        txt_lname = findViewById(R.id.txt_lname);
        txt_fname.setText(u.getFirstName());
        txt_lname.setText(u.getLastName());

    }

}
