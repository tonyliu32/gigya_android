package com.pactera.firstgigyademo;

public interface IGigyaEvents {

    enum EventType {LOGIN, LOGOUT, LOAD, CLOSE, ERROR}

    void onEvent(EventType eventType);
}
