package com.pactera.firstgigyademo.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.pactera.firstgigyademo.R;
import com.pactera.firstgigyademo.model.User;
import com.pactera.firstgigyademo.ui.UIResponse;
import com.pactera.firstgigyademo.ui.managers.RegistrationManager;
import com.pactera.firstgigyademo.ui.viewmodel.AccountViewModel;

public class WelcomeActivity extends AppCompatActivity {

    private Button myaccountButton;
    private TextView txt_name;

    private AccountViewModel mAccountViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        myaccountButton = (Button) findViewById(R.id.btn_myaccount);
        txt_name = (TextView)findViewById(R.id.txt_name);
        mAccountViewModel = ViewModelProviders.of(this).get(AccountViewModel.class);
        setObserver();
        getUserData();

        myaccountButton.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                showMyaccountActivity();
            }
        });
    }

    private void showMyaccountActivity() {

        Intent myaccountIntent = new Intent(this, MyAccountActivity.class);
        myaccountIntent.putExtra("user",mAccountViewModel.getCurrentUser());
        startActivity(myaccountIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void getUserData() {

        mAccountViewModel.getAccountDetails();
    }



    private void setObserver() {


        mAccountViewModel.getAccountLiveData().observe(this, new Observer<UIResponse>() {

            @Override
            public void onChanged(@Nullable UIResponse uiResponse) {

                switch (uiResponse.getResponseType()) {
                    case SUCCESS:
                        if (uiResponse.getData() != null) {
                            User u = (User) uiResponse.getData();
                            String name = u.getFirstName() + u.getLastName();
                            txt_name.setText(name);

                        }
                        break;

                    case IN_PROGRESS:
                        break;

                    case ERROR:
                        break;
                }
            }
        });


    }
}
