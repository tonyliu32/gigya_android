package com.pactera.firstgigyademo.api;



import com.pactera.firstgigyademo.model.PurchaseBody;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface IOfirStoreAPI {


    @POST("/purchase")
    Call<OfirResponse> purchaseItem(@Body PurchaseBody body);


}
