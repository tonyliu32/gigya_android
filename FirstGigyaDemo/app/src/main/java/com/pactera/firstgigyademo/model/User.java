package com.pactera.firstgigyademo.model;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private static final String TAG = "User";

    private String mUID;
    private String mFirstName;
    private String mLastName;

    public User(String firstName, String lastName, String uid){
        mFirstName = firstName;
        mLastName = lastName;
        mUID = uid;
    }

    protected User(Parcel in) {
        mUID = in.readString();
        mFirstName = in.readString();
        mLastName = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel in) {
            return new User(in);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };

    public String getFirstName() {
        return mFirstName;
    }

    public String getLastName() {
        return mLastName;
    }

    public String getUID() {
        return mUID;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mUID);
        dest.writeString(mFirstName);
        dest.writeString(mLastName);

    }
}
