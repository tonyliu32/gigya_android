package com.pactera.firstgigyademo;

public interface ICommand {
    void execute();
}
