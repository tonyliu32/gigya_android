package com.pactera.firstgigyademo.ui.activity;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.gigya.socialize.android.log.GigyaLog;
import com.pactera.firstgigyademo.GigyaSampleLog;
import com.pactera.firstgigyademo.IGigyaEvents;
import com.pactera.firstgigyademo.R;
import com.pactera.firstgigyademo.ui.UIResponse;
import com.pactera.firstgigyademo.ui.managers.RegistrationManager;
import com.pactera.firstgigyademo.ui.viewmodel.AccountViewModel;
import com.pactera.firstgigyademo.ui.viewmodel.LoginViewModel;

public class InitActivity extends AppCompatActivity {

    private static final String TAG = "InitActivity";

    private LoginViewModel mLoginViewModel;
    private Button loginButton;
    final String apikey = "3_1KhvryU-Ry9268o7JaH8FRupysaZ3vsM_HEvnT-g_-t2rXMN_d8fCa0a0fPcq49K";
    final String dataCenter="us1.gigya.com";

    private TextView mProgressText;

    private LinearLayout mProgressLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_init);
        mLoginViewModel = ViewModelProviders.of(this).get(LoginViewModel.class);
        loginButton = (Button) findViewById(R.id.btn_login);
        mProgressText = findViewById(R.id.progress_bar_message);
        mProgressLayout = findViewById(R.id.progress_bar_layout);

        RegistrationManager.getInstance().initialize(getApplication(), apikey,dataCenter);
        setObserver();
        //show gigya popup
        mLoginViewModel.showPlugin("en", "Default");



    }

    private void setObserver() {
        GigyaSampleLog.i(TAG, "setObserver");
        mLoginViewModel.getLoginLiveData().observe(this, new Observer<UIResponse>() {
            @Override
            public void onChanged(@Nullable UIResponse uiResponse) {
                switch (uiResponse.getResponseType()) {
                    case SUCCESS:
                        GigyaLog.i(TAG, "SUCCESS");
                        showWelcomeActivity();
                        hideProgressBar();
                        break;

                    case IN_PROGRESS:
                        GigyaLog.i(TAG, "--------------IN_PROGRESS-----------------------------");

                        showProgressBar(R.string.dialog_purchase_item);
                        break;

                    case ERROR:
                        GigyaLog.i(TAG, "ERROR");
                        break;
                }
            }
        });

//        //add event for showing welcome page
//        RegistrationManager.getInstance().addObserver(new IGigyaEvents() {
//            @Override
//            public void onEvent(EventType eventType) {
//                RegistrationManager.getInstance().removeObserver(this);
//                if(eventType == EventType.LOGIN){
//
//
//                    showWelcomeActivity();
//                }else{
//
//                }
//            }
//        });
    }

    private void showWelcomeActivity() {

        Intent welcomeIntent = new Intent(this, WelcomeActivity.class);
        startActivity(welcomeIntent);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
    }

    private void showProgressBar(int messageID) {
        mProgressText.setText(messageID);
        mProgressLayout.setVisibility(View.VISIBLE);
    }

    private void hideProgressBar() {
        mProgressLayout.setVisibility(View.GONE);
    }



}
